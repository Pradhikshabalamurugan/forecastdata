import streamlit as st
import pandas as pd
import numpy as np
from statsmodels.tsa.arima.model import ARIMA

# Example dataset (replace with your actual data)
timestamps = pd.date_range(start='2020-01-01', end='2024-01-01', freq='D')
values = np.random.randint(10000, 50001, len(timestamps))  # Random values between 10000 and 50000

# Create a DataFrame from the data
data = pd.DataFrame({'timestamp': timestamps, 'value': values})
data.set_index('timestamp', inplace=True)

# Train ARIMA model
model = ARIMA(data['value'], order=(5,1,0))  # Example order, you can tune this
model_fit = model.fit()

# Streamlit web app
st.title('Value Prediction App')

# Date input from the user
input_date = st.date_input('Select a date')

if st.button('Predict'):
    # Predict value for the input date
    future_timestamp = pd.Timestamp(input_date)
    forecast = model_fit.get_forecast(steps=1)
    predicted_value = forecast.predicted_mean.values[0]

    st.write(f'Predicted value for {future_timestamp.date()} is {predicted_value:.2f}')
